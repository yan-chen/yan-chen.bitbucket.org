'use strict';

/**
 * @ngdoc overview
 * @name drwentzApp
 * @description
 * # drwentzApp
 *
 * Main module of the application.
 */
angular
    .module('drwentzApp', [
        'ngAnimate',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.bootstrap',
        'pascalprecht.translate'
    ])
    .config(function ($routeProvider, $sceDelegateProvider, $translateProvider, $locationProvider, $httpProvider) {

        $sceDelegateProvider.resourceUrlWhitelist([
            'self',                    // trust all resources from the same origin
            '*://www.youtube.com/**'   // trust all resources from `www.youtube.com`
        ]);

        $translateProvider.useStaticFilesLoader({
            prefix: 'assets/translations/',
            suffix: '.json'
        });
        $translateProvider.preferredLanguage('en');
        $translateProvider.fallbackLanguage('en');
        $translateProvider.useSanitizeValueStrategy('sceParameters');
        $translateProvider.useMissingTranslationHandler('myCustomHandlerFactory');

        //$locationProvider.html5Mode(true).hashPrefix('!');
        $locationProvider.hashPrefix('!');

        $routeProvider
            .when('/home/:lang?', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'Home',
                metaData: {
                    'pageTitle': 'HOME'
                },
                resolve: {
                    slides: function ($http) {
                        return $http.get('assets/data/homeSlides.json').then(function (response) {
                            return response.data.images;
                        });
                    },
                    menu: function ($http, $rootScope) {
                        return $http.get('assets/data/navLinks.json').then(function (response) {
                            return response.data.slice(1);
                        });
                    }
                }
            })
            .when('/humanitarian/:lang?', {
                templateUrl: 'views/humanitarian.html',
                controller: 'HumanitarianCtrl',
                controllerAs: 'Humanitarian',
                metaData: {
                    'pageTitle': 'HUMANITARIAN'
                },
                resolve: {
                    slides: function ($http) {
                        return $http.get('assets/data/humanitarianSlides.json')
                            .then(function (response) {
                                return response.data.images;
                            });
                    }
                }
            })
            .when('/press/:lang?', {
                templateUrl: 'views/press.html',
                controller: 'PressCtrl',
                controllerAs: 'Press',
                metaData: {
                    'pageTitle': 'PRESS'
                }
            })
            .when('/publications/:lang?', {
                templateUrl: 'views/publications.html',
                controller: 'PublicationsCtrl',
                controllerAs: 'Publications',
                metaData: {
                    'pageTitle': 'PUBLICATIONS'
                }
            })
            .when('/scientist/:lang?', {
                templateUrl: 'views/scientist.html',
                controller: 'ScientistCtrl',
                controllerAs: 'Scientist',
                metaData: {
                    'pageTitle': 'SCIENTIST'
                },
                resolve: {
                    slides: function ($http) {
                        return $http.get('assets/data/scientistSlides.json')
                            .then(function (response) {
                                return response.data.images;
                            });
                    }
                }
            })
            .when('/videos/:lang?', {
                templateUrl: 'views/videos.html',
                controller: 'VideosCtrl',
                controllerAs: 'Videos',
                metaData: {
                    'pageTitle': 'VIDEOS'
                },
                resolve: {
                    videoList: function ($http) {
                        return $http.get('assets/data/videos.json')
                            .then(function (response) {
                                return response.data;
                            });
                    }
                }
            })
            .when('/vision/:lang?', {
                templateUrl: 'views/vision.html',
                controller: 'VisionCtrl',
                controllerAs: 'Vision',
                metaData: {
                    'pageTitle': 'VISION'
                }
            });

        $routeProvider.otherwise({redirectTo: '/home'});

    })

    .run(function ($rootScope, $route, $translate, $location) {

        $rootScope.$on('$routeChangeStart', function (e, current) {
            var lang = '';
            var locationPath = /[^/]*\/!$/.exec($location.path());

            if (current.params.lang !== undefined || null) {
                lang=current.params.lang.toLocaleLowerCase()
            } else if (locationPath!== null && locationPath[0] !== (null||'')) {
                lang = locationPath[0];
            } else {
                lang = 'en';
            }
            $translate.use(lang);
            $rootScope.langKey=lang;
        });

        $rootScope.$on('$routeChangeSuccess', function () {
            $rootScope.$broadcast('Route Reloaded');
        });

    });
