'use strict';

/**
 * @ngdoc directive
 * @name drwentzApp.directive:footer
 * @description
 * # footer
 */
angular.module('drwentzApp')
  .directive('footer', function () {
    return {
      templateUrl: 'views/directive.footer.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {

      }
    };
  });
