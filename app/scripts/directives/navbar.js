'use strict';

/**
 * @ngdoc directive
 * @name drwentzApp.directive:navbar
 * @description
 * # navbar
 */
angular.module('drwentzApp')
    .directive('navbar', function () {
        return {
            templateUrl: 'views/directive.navbar.html',
            replace: true,
            controller: function ($scope,$http, $translate, $route, $filter,$location,$routeParams) {

                var _this = this;
                _this.isCollapsed = true;
                _this.languages=[];
                _this.currentLanguage={};

                $http.get('assets/data/navLinks.json')
                    .then(function (response) {
                        _this.menu = response.data;
                    });

                $http.get('assets/data/languages.json')
                    .then(function (response) {
                        _this.languages = response.data;
                        _this.currentLanguage = $filter('filter')(_this.languages, {'key': $route.current.params.lang})[0];
                    });

                _this.changeLanguage = function (lang) {
                    //var currentLocationPath=$location.path().toString();
                    //if($routeParams.lang!==undefined||null){
                    //    var newLocationPath = currentLocationPath.replace(/[^/]*\/*$/,lang.key);
                    //    $location.path(newLocationPath);
                    //    console.log($location.path()+' from if')
                    //
                    //}else{
                    //    $location.path(currentLocationPath+'/'+lang.key);
                    //    console.log($location.path()+' from else')
                    //}

                    $translate.use(lang.key);
                    _this.currentLanguage = lang;
                };

                $scope.$on('Route Reloaded',function(){
                    _this.currentLanguage = $filter('filter')(_this.languages, {'key': $route.current.params.lang})[0];
                });
            },
            controllerAs: 'nav',
            restrict: 'E'
            //,
            //link: function postLink(scope, element, attrs) {
            //
            //}
        };
    });
