'use strict';

/**
 * @ngdoc service
 * @name drwentzApp.modal
 * @description
 * # modal
 * Service in the drwentzApp.
 */
angular.module('drwentzApp')
    .service('modal', function ($document, $uibModal,$log) {


        var ModalService = {

            open: function (templateName, item, size, parentSelector) {

                var parentElem = parentSelector ?
                    angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: templateName,
                    controller: 'ModalInstanceCtrl',
                    controllerAs: 'ModalCtrl',
                    size: size,
                    appendTo: parentElem,
                    resolve: {
                        item: function () {
                            return item;
                        }
                    }
                });

                modalInstance.result.then(function () {
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            }
        };

        return ModalService;

    })

    .controller('ModalInstanceCtrl', function ($uibModalInstance, item) {
        var $ctrl = this;
        $ctrl.item = item;

        $ctrl.ok = function () {
            $uibModalInstance.close();
        };

        $ctrl.$dismiss = function () {
            $uibModalInstance.dismiss('cancel');
        };

    });