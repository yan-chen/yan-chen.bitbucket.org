'use strict';

/**
 * @ngdoc service
 * @name drwentzApp.metaService
 * @description
 * # metaService
 * Service in the drwentzApp.
 */
angular.module('drwentzApp')
    .service('metaService', function ($filter, $location) {
        // AngularJS will instantiate a singleton by calling "new" on this function


        var metaDescription = 'EXPLORE THE OFFICIAL SITE ...';

        return {
            pageTitle: function () {
                return $filter('translate')('DR MYRON WENTZ') + ' - ' + ($filter('translate')($location.path().replace('/', '').toUpperCase()) || ' Humanitarian, Visionary and Scientist');
            },
            metaDescription: function () {
                return $filter('translate')(metaDescription);
            },
            resetMeta: function () {
                metaDescription = '';
            },
            setMetaDescription: function (newMetaDescription) {
                metaDescription = newMetaDescription;
            }
        };


    });
