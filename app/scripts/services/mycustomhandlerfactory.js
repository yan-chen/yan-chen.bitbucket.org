'use strict';

/**
 * @ngdoc service
 * @name drwentzApp.myCustomHandlerFactory
 * @description
 * # myCustomHandlerFactory
 * Factory in the drwentzApp.
 */
angular.module('drwentzApp')
    .factory('myCustomHandlerFactory', function ($location) {
        // Service logic
        // ...

        // Public API here
        return function (translateID, uses) {
            //console.log(translateID);
            //console.log(uses);
            //$location.path('/home');
        };
    });
