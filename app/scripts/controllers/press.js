'use strict';

/**
 * @ngdoc function
 * @name drwentzApp.controller:PressCtrl
 * @description
 * # PressCtrl
 * Controller of the drwentzApp
 */
angular.module('drwentzApp')
  .controller('PressCtrl', function () {
      var _this = this;

      _this.imagesCol1 = [
          {
              'id': '0',
              'url': 'assets/images/press/1.jpg',
              'caption': 'THE HEALTHY HOME BOOK TOUR ARRIVES IN NEW YORK CITY'
          },
          {
              'id': '1',
              'url': 'assets/images/press/2.jpg',
              'caption': 'RINGING THE BELL AT THE NEW YORK STOCK EXCHANGE'
          },
          {
              'id': '2',
              'url': 'assets/images/press/3.jpg',
              'caption': ''
          }
      ];

      _this.imagesCol2 = [
          {
              'id': '3',
              'url': 'assets/images/press/4.jpg',
              'caption': 'BOOK SIGNING'
          },
          {
              'id': '4',
              'url': 'assets/images/press/5.jpg',
              'caption': 'BOOK SIGNING'
          },
          {
              'id': '5',
              'url': 'assets/images/press/6.jpg',
              'caption': ''
          }

      ];

      _this.imagesCol3 = [
          {
              'id': '6',
              'url': 'assets/images/press/7.jpg',
              'caption': 'THE HEALTHY HOME BECOMES A NEW YORK TIMES BEST SELLER'
          },
          {
              'id': '7',
              'url': 'assets/images/press/8.jpg',
              'caption': 'DR WENTS WITH HIS SON, DAVE WENTZ, ...'
          },
          {
              'id': '8',
              'url': 'assets/images/press/9.jpg',
              'caption': ''
          },
          {
              'id': '9',
              'url': 'assets/images/press/10.jpg',
              'caption': 'BOOK SIGNING'
          }

      ];

  });
