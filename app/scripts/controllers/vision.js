'use strict';

/**
 * @ngdoc function
 * @name drwentzApp.controller:VisionCtrl
 * @description
 * # VisionCtrl
 * Controller of the drwentzApp
 */
angular.module('drwentzApp')
    .controller('VisionCtrl', function () {

        var _this = this;

        _this.imagesCol1 = [
            {
                'id': '0',
                'url': 'assets/images/vision/convention.jpg',
                'caption': 'USANA INTERNATIONAL CONVENTION'
            },
            {
                'id': '1',
                'url': 'assets/images/vision/home_office.jpg',
                'caption': 'USANA HOME OFFICES IN SALT LAKE CITY UTAH'
            },
            {
                'id': '2',
                'url': 'assets/images/vision/sanoviv_med_lab.jpg',
                'caption': 'SANOVIV MEDICAL INSTITUTE LABORATORY'
            }
        ];

        _this.imagesCol2 = [
            {
                'id': '3',
                'url': 'assets/images/vision/droz.jpg',
                'caption': 'DR MEHMET OZ AND DAVE WENTZ'
            },
            {
                'id': '4',
                'url': 'assets/images/vision/sanoviv.jpg',
                'caption': 'SANOVIV MEDICAL INSTITUTE'
            }

        ];

        _this.imagesCol3 = [
            {
                'id': '5',
                'url': 'assets/images/vision/wentz_conccert_hall.jpg',
                'caption': 'THE WENTZ CONCERT HALL AT NORTH CENTRAL COLLEGE'
            },
            {
                'id': '6',
                'url': 'assets/images/vision/breakingground.jpg',
                'caption': 'BREAKING GROUND AT USANA HOME OFFICE'
            },
            {
                'id': '7',
                'url': 'assets/images/vision/distributing_supplements.jpg',
                'caption': 'DISTRIBUTING USANA SUPPLEMENTS IN EI SALVADOR'
            }

        ];
    });
