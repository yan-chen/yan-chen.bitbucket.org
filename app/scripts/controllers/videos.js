'use strict';

/**
 * @ngdoc function
 * @name drwentzApp.controller:VideosCtrl
 * @description
 * # VideosCtrl
 * Controller of the drwentzApp
 */
angular.module('drwentzApp')
    .controller('VideosCtrl', function (videoList, modal) {

        var _this = this;
        _this.videoList = videoList;
        _this.openVideoModal = modal.open;

    });
