'use strict';

/**
 * @ngdoc function
 * @name drwentzApp.controller:ScientistCtrl
 * @description
 * # ScientistCtrl
 * Controller of the drwentzApp
 */
angular.module('drwentzApp')
    .controller('ScientistCtrl', function (slides) {
        var _this = this;
        _this.active = 0;
        _this.slides = slides;

    });
