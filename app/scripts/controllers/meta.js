'use strict';

/**
 * @ngdoc function
 * @name drwentzApp.controller:MetaCtrl
 * @description
 * # MetaCtrl
 * Controller of the drwentzApp
 */
angular.module('drwentzApp')
    .controller('MetaCtrl', function (metaService) {

        var _this = this;
        _this.meta = metaService;

    });
