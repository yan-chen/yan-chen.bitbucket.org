'use strict';

/**
 * @ngdoc function
 * @name drwentzApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the drwentzApp
 */
angular.module('drwentzApp')
    .controller('MainCtrl', function (slides, menu) {
        var _this = this;
        _this.active = 0;
        _this.slides = slides;
        _this.menu = menu;
    });
