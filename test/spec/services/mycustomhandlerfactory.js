'use strict';

describe('Service: myCustomHandlerFactory', function () {

  // load the service's module
  beforeEach(module('drwentzApp'));

  // instantiate service
  var myCustomHandlerFactory;
  beforeEach(inject(function (_myCustomHandlerFactory_) {
    myCustomHandlerFactory = _myCustomHandlerFactory_;
  }));

  it('should do something', function () {
    expect(!!myCustomHandlerFactory).toBe(true);
  });

});
