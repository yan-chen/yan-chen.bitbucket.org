'use strict';

describe('Controller: ScientistCtrl', function () {

  // load the controller's module
  beforeEach(module('drwentzApp'));

  var ScientistCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ScientistCtrl = $controller('ScientistCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ScientistCtrl.awesomeThings.length).toBe(3);
  });
});
