'use strict';

describe('Controller: MetaCtrl', function () {

  // load the controller's module
  beforeEach(module('drwentzApp'));

  var MetaCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MetaCtrl = $controller('MetaCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MetaCtrl.awesomeThings.length).toBe(3);
  });
});
