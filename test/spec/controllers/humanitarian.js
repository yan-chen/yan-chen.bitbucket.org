'use strict';

describe('Controller: HumanitarianCtrl', function () {

  // load the controller's module
  beforeEach(module('drwentzApp'));

  var HumanitarianCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    HumanitarianCtrl = $controller('HumanitarianCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(HumanitarianCtrl.awesomeThings.length).toBe(3);
  });
});
