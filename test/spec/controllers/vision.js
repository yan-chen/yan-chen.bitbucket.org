'use strict';

describe('Controller: VisionCtrl', function () {

  // load the controller's module
  beforeEach(module('drwentzApp'));

  var VisionCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    VisionCtrl = $controller('VisionCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(VisionCtrl.awesomeThings.length).toBe(3);
  });
});
